//
//  BreatheView.swift
//  BreatheMacOS
//
//  Created by Andrey Mishanin on 17/12/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Cocoa

final class BreatheView: NSView {
  private let petals = (0..<numPetals).map {
    petalColours[$0]
  }.map {
    makePetalLayer(colour: $0)
  }
  private let flower = CALayer()
  private let petalInstance = makePetalLayer(
    colour: petalColours[0],
    size: petalSize.applying(s)
  )
  private let flowerCopy = makeFlowerCopyLayer()

  private var inDebugMode = false
  // Debug UI
  private var slider: NSSlider!
  private let keyTimeLabel = NSTextField(labelWithString: "Key Time: ")
  private let timeLabel = NSTextField(labelWithString: "Time: ")

  override init(frame frameRect: NSRect) {
    super.init(frame: frameRect)
  }

  required init?(coder decoder: NSCoder) {
    super.init(coder: decoder)
    wantsLayer = true
    layer!.backgroundColor = Colour.black.cgColor

    flowerCopy.filters = [makeBlurFilter()]
    flowerCopy.addSublayer(petalInstance)
    layer!.addSublayer(flowerCopy)
    petals.forEach(flower.addSublayer)
    layer!.addSublayer(flower)

    addAnimations()

    if inDebugMode {
      slider = NSSlider(value: 0, minValue: 0, maxValue: totalDuration, target: self, action: #selector(sliderChanged))
      addSubview(slider)
      addSubview(keyTimeLabel)
      addSubview(timeLabel)
      slider.sendAction(slider.action, to: slider.target)
      showLayers()
      stopTime()
    }
  }

  private func addAnimations() {
    for (idx, petal) in petals.enumerated() {
      petal.add(makeAnimationForPetal(at: idx), forKey: "PA")
    }
    flower.add(makeAnimationForFlower(), forKey: "FA")
    flowerCopy.add(makeAnimationForFlowerCopy(), forKey: "FCA")
  }

  override func layout() {
    super.layout()
    flower.position = bounds.centre
    flower.bounds = CGRect(origin: .zero, size: flowerSize)
    petals.forEach {
      $0.position = flower.bounds.centre
      $0.bounds = CGRect(origin: .zero, size: petalSize)
    }

    flowerCopy.position = bounds.centre
    flowerCopy.bounds = CGRect(origin: .zero, size: flowerSize)
    petalInstance.position = flowerCopy.bounds.centre.applying(
      CGAffineTransform(translationX: 2 * petalRadius, y: 0)
    )
    petalInstance.bounds = CGRect(origin: .zero, size: petalSize.applying(s))

    if inDebugMode {
      slider.frame = CGRect(x: 0, y: 0, width: bounds.width, height: 19)
      let labelHeight = 19 as CGFloat
      keyTimeLabel.frame = CGRect(x: 0, y: bounds.maxY - labelHeight, width: bounds.width, height: labelHeight)
      timeLabel.frame = CGRect(x: 0, y: keyTimeLabel.frame.minY - labelHeight, width: bounds.width, height: labelHeight)
    }
  }

  // Debug Mode

  @objc func sliderChanged() {
    keyTimeLabel.stringValue = "Key Time: \(slider.doubleValue / totalDuration)"
    timeLabel.stringValue = "Time: \(slider.doubleValue)"
    setTimeOffset(slider.doubleValue)
  }

  private func showLayers() {
    showLayer(flower)
    petals.forEach(showLayer)
    showLayer(flowerCopy)
    showLayer(petalInstance)
  }

  private func stopTime() {
    stopTimeIn(flower)
    petals.forEach(stopTimeIn)
    stopTimeIn(flowerCopy)
  }

  private func setTimeOffset(_ offset: CFTimeInterval) {
    flower.timeOffset = offset
    petals.forEach { $0.timeOffset = offset }
    flowerCopy.timeOffset = offset
  }

}

private func makePetalLayer(colour: Colour, size: CGSize = petalSize) -> CALayer {
  let l = CAShapeLayer()
  l.fillColor = colour.withAlphaComponent(petalOpacity).cgColor
  l.path = CGPath(ellipseIn: CGRect(origin: .zero, size: size),
                  transform: nil)
  return l
}

private func makeFlowerCopyLayer() -> CALayer {
  let l = CAReplicatorLayer()
  l.instanceCount = numPetals
  l.instanceTransform = CATransform3DMakeRotation(segmentAngle, 0, 0, 1)
  let redOffset = (topRightColour.redComponent - bottomLeftColour.redComponent) / CGFloat(numPetals)
  l.instanceRedOffset = Float(redOffset)

  let greenOffset = (topRightColour.greenComponent - bottomLeftColour.greenComponent) / CGFloat(numPetals)
  l.instanceGreenOffset = Float(greenOffset)

  let blueOffset = (topRightColour.blueComponent - bottomLeftColour.blueComponent) / CGFloat(numPetals)
  l.instanceBlueOffset = Float(blueOffset)
  return l
}

private let s = CGAffineTransform(scaleX: finalScale, y: finalScale)
private let totalDuration = duration + delayBetweenInhaleAndExhale + duration
private enum KeyTimes {
  static let inhale = duration / totalDuration
  static let exhale = (duration + delayBetweenInhaleAndExhale) / totalDuration
  static let copyStartBlur = (duration + delayBetweenInhaleAndExhale + 0.4 * duration) / totalDuration
  static let copyFadeOut = (duration + delayBetweenInhaleAndExhale + 0.95 * duration) / totalDuration
}

private func makeAnimationForPetal(at petalIdx: Int) -> CAAnimation {
  let a = makeKeyframeAnimation(of: AnimatableProperties.Transform.self, with: [
    (0, CATransform3DIdentity),
    (KeyTimes.inhale, transform3DForPetal(at: petalIdx)),
    (KeyTimes.exhale, transform3DForPetal(at: petalIdx)),
    (1, CATransform3DIdentity)
  ])
  applyCommonParams(to: a)
  return a
}

private func makeAnimationForFlower() -> CAAnimation {
  let a = makeKeyframeAnimation(of: AnimatableProperties.Transform.self, with: [
    (0, CATransform3DIdentity),
    (KeyTimes.inhale, CATransform3DMakeRotation(-2 * segmentAngle, 0, 0, 1)),
    (KeyTimes.exhale, CATransform3DMakeRotation(-2 * segmentAngle, 0, 0, 1)),
    (1, CATransform3DIdentity)
  ])
  applyCommonParams(to: a)
  return a
}

private func makeAnimationForFlowerCopy() -> CAAnimation {
  let timingFunctions = [ CAMediaTimingFunction(name: .easeIn) ]
  let opacity = makeKeyframeAnimation(of: AnimatableProperties.Opacity.self, with: [
    (KeyTimes.inhale, 0),
    (KeyTimes.exhale, 0.8),
    (KeyTimes.copyFadeOut, 0),
    (1, 0)
  ])
  opacity.timingFunctions = timingFunctions

  let rotation = makeKeyframeAnimation(of: AnimatableProperties.Transform.self, with: [
    (KeyTimes.exhale, CATransform3DIdentity),
    (1, CATransform3DMakeRotation(2 * segmentAngle, 0, 0, 1))
  ])
  rotation.timingFunctions = timingFunctions

  let scale = makeKeyframeAnimation(of: AnimatableProperties.Transform.self, with: [
    (KeyTimes.exhale, CATransform3DIdentity),
    (1, CATransform3DMakeScale(0.8, 0.8, 1))
  ])
  scale.timingFunctions = timingFunctions
  // We want to apply both animations to the same key path (transform)
  scale.isAdditive = true

  let blur = makeKeyframeAnimation(of: AnimatableProperties.BlurRadius.self, with: [
    (KeyTimes.copyStartBlur, 0),
    (KeyTimes.copyFadeOut, maxBlurRadius),
    (1, maxBlurRadius)
  ])

  let a = CAAnimationGroup()
  a.animations = [opacity, rotation, scale, blur]
  a.duration = totalDuration
  a.repeatCount = .greatestFiniteMagnitude
  return a
}

private func makeBlurFilter() -> CIFilter {
  let f = CIFilter(name: "CIGaussianBlur")!
  f.setDefaults()
  f.setValue(0, forKey: "inputRadius")
  f.name = filterName
  return f
}

private func applyCommonParams(to a: CAKeyframeAnimation) {
  a.timingFunctions = [
    CAMediaTimingFunction(name: .easeOut),
    CAMediaTimingFunction(name: .linear),
    CAMediaTimingFunction(name: .easeIn),
  ]
  a.duration = totalDuration
  a.repeatCount = .greatestFiniteMagnitude
}

private func showLayer(_ layer: CALayer) {
  layer.borderWidth = 1
  layer.borderColor = Colour.white.cgColor
}

private func stopTimeIn(_ layer: CALayer) {
  layer.speed = 0
}

private extension CGRect {
  var centre: CGPoint { return CGPoint(x: midX, y: midY) }
}

