//
//  KeyframeAnimations.swift
//  BreatheMacOS
//
//  Created by Andrey Mishanin on 20/12/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation
import QuartzCore

protocol AnimatableProperty {
  associatedtype ValueType
  static var keyPath: String { get }
}

enum AnimatableProperties {
  struct Transform: AnimatableProperty {
    typealias ValueType = CATransform3D
    static var keyPath = "transform"
  }
  struct Opacity: AnimatableProperty {
    typealias ValueType = Double
    static var keyPath = "opacity"
  }
  struct BlurRadius: AnimatableProperty {
    typealias ValueType = Double
    static var keyPath = "filters.\(filterName).inputRadius"
  }
}

typealias KeyTimesWithValues<T> = [(Double, T)]
func makeKeyframeAnimation<P: AnimatableProperty>(of property: P.Type, with keyTimesWithValues: KeyTimesWithValues<P.ValueType>) -> CAKeyframeAnimation {
  let a = CAKeyframeAnimation(keyPath: property.keyPath)
  a.keyTimes = keyTimesWithValues.map { $0.0 } as [NSNumber]
  a.values = keyTimesWithValues.map { $0.1 }
  return a
}
