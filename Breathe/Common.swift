//
//  Common.swift
//  Breathe
//
//  Created by Andrey Mishanin on 15/12/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import CoreGraphics
import QuartzCore

#if os(macOS)
import AppKit
typealias Colour = NSColor
#else
import UIKit
typealias Colour = UIColor
#endif

let numPetals = 6
let topRightColour = Colour(r: 140, g: 232, b: 186)
let bottomLeftColour = Colour(r: 110, g: 191, b: 213)
let petalColours: [Colour] = {
  let fractions = [0.66, 1, 0.66, 0.33, 0, 0.33] as [CGFloat]
  precondition(fractions.count == numPetals)
  return fractions.map { interpolate(between: topRightColour, and: bottomLeftColour, fraction: $0) }
}()
let petalOpacity = 0.5 as CGFloat
let petalRadius = 25 as CGFloat
let petalSize = CGSize(width: petalRadius * 2, height: petalRadius * 2)
let finalScale = 2 as CGFloat
let segmentAngle = 2 * CGFloat.pi / CGFloat(numPetals)
let duration = 3 as CFTimeInterval
let delayBetweenInhaleAndExhale = 1 as CFTimeInterval
let flowerSize: CGSize = {
  let maxFlowerDimension = 2 * petalRadius * finalScale * 2
  return CGSize(width: maxFlowerDimension, height: maxFlowerDimension)
}()
let filterName = "blur"
let maxBlurRadius = 15.0

func transformForPetal(at petalIdx: Int) -> CGAffineTransform {
  let angle = segmentAngle * CGFloat(petalIdx)
  let flyout = petalRadius
  let translation = CGAffineTransform(translationX: flyout * cos(angle),
                                      y: flyout * sin(angle))
  let scale = CGAffineTransform(scaleX: finalScale, y: finalScale)
  return translation.concatenating(scale)
}

func transform3DForPetal(at petalIdx: Int) -> CATransform3D {
  let α = segmentAngle * CGFloat(petalIdx)
  let flyout = petalRadius
  let translation = CATransform3DMakeTranslation(flyout * cos(α),
                                                 flyout * sin(α),
                                                 0)
  let scale = CATransform3DMakeScale(finalScale, finalScale, 1)
  return CATransform3DConcat(translation, scale)
}

private func interpolate(between c1: Colour, and c2: Colour, fraction: CGFloat) -> Colour {
  let c1RGBA = c1.rgba
  let c2RGBA = c2.rgba
  return Colour(red: interpolate(between: c1RGBA.red, and: c2RGBA.red, fraction: fraction),
                green: interpolate(between: c1RGBA.green, and: c2RGBA.green, fraction: fraction),
                blue: interpolate(between: c1RGBA.blue, and: c2RGBA.blue, fraction: fraction),
                alpha: interpolate(between: c1RGBA.alpha, and: c2RGBA.alpha, fraction: fraction))
}

private func interpolate(between a: CGFloat, and b: CGFloat, fraction: CGFloat) -> CGFloat {
  return a + (b - a) * fraction
}

extension Colour {
  struct RGBA {
    let red: CGFloat
    let green: CGFloat
    let blue: CGFloat
    let alpha: CGFloat
  }

  convenience init(r: UInt8, g: UInt8, b: UInt8) {
    self.init(red: CGFloat(r) / CGFloat(UInt8.max),
              green: CGFloat(g) / CGFloat(UInt8.max),
              blue: CGFloat(b) / CGFloat(UInt8.max),
              alpha: 1)
  }

  var rgba: RGBA {
    var r = 0 as CGFloat
    var g = 0 as CGFloat
    var b = 0 as CGFloat
    var a = 0 as CGFloat
    getRed(&r, green: &g, blue: &b, alpha: &a)
    return RGBA(red: r, green: g, blue: b, alpha: a)
  }
}

