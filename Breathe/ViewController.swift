//
//  ViewController.swift
//  Breathe
//
//  Created by Andrey Mishanin on 01/12/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import UIKit

final class ViewController: UIViewController, CAAnimationDelegate {
  private let petals = (0..<numPetals).map { PetalView(colour: petalColours[$0]) }
  private let flower = UIView(frame: .zero)
  private let flowerSnapshot = UIImageView(frame: .zero)
  private let blurView = UIVisualEffectView(frame: .zero)

  override func viewDidLoad() {
    super.viewDidLoad()

    flowerSnapshot.addSubview(blurView)
    view.addSubview(flowerSnapshot)

    petals.forEach(flower.addSubview)
    view.addSubview(flower)

//    showViewBounds(in: flower)

    flower.frame = flowerSize.centered(in: view.bounds)
    for p in petals {
      p.frame = petalSize.centered(in: flower.bounds)
    }
  }

  private func showViewBounds(in view: UIView) {
    view.layer.borderWidth = 1
    view.layer.borderColor = UIColor.white.cgColor
    view.subviews.forEach(showViewBounds)
  }

  override func viewWillLayoutSubviews() {

  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    runAnimations()
  }

  func runAnimations() {
    UIView.animate(withDuration: duration,
                   delay: 0,
                   options: [.curveEaseOut],
                   animations: self.inhaleAnimation,
                   completion: { _ in self.exhaleAnimation() })
  }

  private func inhaleAnimation() {
    for (idx, p) in petals.enumerated() {
      p.transform = transformForPetal(at: idx)
    }
    flower.transform = CGAffineTransform(rotationAngle: segmentAngle)
  }

  private func exhaleAnimation() {
    flowerSnapshot.image = makeFlowerSnapshot()
    flowerSnapshot.alpha = 0.8
    flowerSnapshot.transform = .identity
    flowerSnapshot.frame = flowerSize.centered(in: view.bounds)
    blurView.frame = flowerSnapshot.bounds
    blurView.effect = nil

    UIView.animate(withDuration: duration / 2,
                   delay: 1 + duration / 3,
                   animations: { self.blurView.effect = UIBlurEffect(style: .dark) })
    UIView.animate(withDuration: duration,
                   delay: delayBetweenInhaleAndExhale,
                   animations: {
                    self.flowerSnapshot.alpha = 0
                    self.flowerSnapshot.transform = snapshotTransform
    })
#if false
    flower.isHidden = true
    petals.forEach { $0.isHidden = true }
#else
    UIView.animate(withDuration: duration,
                   delay: delayBetweenInhaleAndExhale,
                   animations: {
                    self.flower.transform = .identity
                    self.petals.forEach { $0.transform = .identity }
    },
                   completion: { _ in self.runAnimations() })
#endif
  }

  private func makeFlowerSnapshot() -> UIImage {
    let r = UIGraphicsImageRenderer(bounds: view.bounds)
    let snapshot = r.image(actions: { context in
      view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
    })
    let r2 = UIGraphicsImageRenderer(size: flowerSize)
    let croppedSnapshot = r2.image(actions: { context in
      let cropRect = flowerSize.centered(in: view.bounds)
      let drawRect = CGRect(x: -cropRect.minX,
                            y: -cropRect.minY,
                            width: snapshot.size.width,
                            height: snapshot.size.height)
      context.clip(to: CGRect(origin: .zero, size: cropRect.size))
      snapshot.draw(in: drawRect)
    })
    return croppedSnapshot
  }
}

private let snapshotTransform: CGAffineTransform = {
  let scale = CGAffineTransform(scaleX: 0.8, y: 0.8)
  return CGAffineTransform(rotationAngle: -segmentAngle).concatenating(scale)
}()

private final class PetalView: UIView {
  override class var layerClass: AnyClass {
    return CAShapeLayer.self
  }

  init(colour: UIColor) {
    super.init(frame: .zero)
    shapeLayer.fillColor = colour.withAlphaComponent(petalOpacity).cgColor
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    shapeLayer.path = CGPath(ellipseIn: bounds,
                             transform: nil)
  }

  private var shapeLayer: CAShapeLayer { return layer as! CAShapeLayer }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

private extension CGSize {
  func centered(in rect: CGRect) -> CGRect {
    let origin = CGPoint(x: rect.midX - width / 2, y: rect.midY - height / 2)
    return CGRect(origin: origin, size: self)
  }
}
