//
//  AppDelegate.swift
//  Breathe
//
//  Created by Andrey Mishanin on 01/12/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  private let vc = ViewController()

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    let w = UIWindow(frame: UIScreen.main.bounds)
    w.rootViewController = vc
    w.makeKeyAndVisible()
    window = w
    return true
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
//    vc.runAnimations()
  }
}
